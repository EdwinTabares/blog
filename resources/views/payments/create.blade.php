
@extends('template.main')
@section('title', $user->nombre)

@section('content')



{!! Form::open(['route' => ['payment.pago', $user->id], 'method' => 'POST']) !!}

<div class="form-group">
	{!! Form::label('valor', 'Valor') !!}
	{!! Form::text('valor', null, ['class' =>'form-control', 'placeholder' => 'ingrese el valor del pago', 'required']) !!}
</div>

<div class="form-group">
	{!! Form::label('fecha', 'fecha') !!}
	{!! Form::date('fecha_pago', null, ['class' =>'form-control', 'placeholder' => 'ingrese la fecha ', 'required']) !!}
</div>


<div class="form-group">
	{!! Form::submit('Registrar', ['class' => 'btn btn-primary']) !!}
</div>

{!! Form::close() !!}


@endsection