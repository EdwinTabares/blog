<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	return view('welcome');
});



Route::name('payment.pagoUsuario')->get('/pagosUsuario/{user}','PaymentController@pagoUsuario');

Route::name('payment.historial')->get('/historial/{user}','PaymentController@historial');

Route::name('payment.pago')->post('/pago/{request}','PaymentController@pago');


Route::resource('users','UserController');
Route::resource('payments','PaymentController');