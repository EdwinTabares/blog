<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\payment;
use App\User;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\PaymentRequest;


class PaymentController extends Controller
{

  public function index()
  {

    $users = DB::table('users as u')
    ->join('payments','u.id','=','payments.user_id' )
    ->select('u.id','u.nombre','u.apellido', DB::raw('sum(valor) as total, count(payments.id) as cantidad_pagos'))->groupBy('u.id','u.nombre','u.apellido')->get();


    return view('payments.index')->with('users',$users);
  }

  public function create()
  {
    return view('payments.create');
  }

  public function store(PaymentRequest $request)
  {

    $payment = new payment($request->all());
    $payment->save();

    return redirect()->route('payment.index');    

  }


  public function pago(PaymentRequest $request, $id)
  { 

    $payment = new payment($request->all());
    $payment->user_id = $id;
    $payment->save();
    return redirect()->route('payments.index');  

  }


  public function pagoUsuario( User $user)
  {

    return view('payments.create')->with('user', $user);
  }


  public function historial($user)
  {
    $pagosU = payment::where('user_id',$user)->paginate(5);

    return view('payments.historial')->with('pagosU',$pagosU);
  }

}
