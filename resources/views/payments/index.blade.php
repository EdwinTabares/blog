@extends('template.main')

@section('title', 'Lista de pagos')



@section('content')
<a href="{{ route('users.create')}}" class="btn btn-info">Registrar nuevo usuario</a>
<table class="table table-striped">
	<thead>
		
		<th>Nombre</th>
		<th>apellido</th>
		<th>cantidad pagos</th>
		<th>sumatoria pagos</th>
		
	</thead>
	<tbody>
		@foreach($users as $user)
		<tr>
			<td>{{ $user->nombre}}</td>
			<td>{{ $user->apellido}}</td>
			<td>{{ $user->cantidad_pagos}}</td>
			<td>{{ $user->total}}</td>
			
			@endforeach
		</tbody>

	</table>

	@endsection()