<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    static $password;


    return [
           'nombre'		=>	$faker->name,
		   'apellido'		=>	$faker->lastname,
		  	'tipo_documento' => 'cedulaC',
			'numero_documento'=>$faker->numberBetween(100000, 1000000),
			'salario' =>$faker->numberBetween(500000, 1000000)
    ];

  return $array;
});
 