<?php

use Faker\Generator as Faker;

$factory->define(App\payment::class, function (Faker $faker) {
    return [
       
		'fecha_pago' => $faker->dateTimeBetween('-2 years', 'now'),
		'valor'=>$faker->numberBetween(500000, 1000000),
		'user_id' => App\User::all()->random()->id

    ];
    return $array;
});
