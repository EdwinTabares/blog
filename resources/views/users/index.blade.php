@extends('template.main')

@section('title', 'Lista de usuarios')



@section('content')
<a href="{{ route('users.create')}}" class="btn btn-info">Registrar nuevo usuario</a>
<table class="table table-striped">
	<thead>
		<th>Id</th>
		<th>Nombre</th>
		<th>apellido</th>
		<th>Tipo_Documento</th>
		<th>Numero_Documento</th>
		<th>Salario</th>
	</thead>
	<tbody>
		@foreach($users as $user)
		<tr>
			<td>{{ $user->id}}</td>
			<td>{{ $user->nombre}}</td>
			<td>{{ $user->apellido}}</td>
			<td>{{ $user->tipo_documento}}</td>
			<td>{{ $user->numero_documento}}</td>
			<td>{{ $user->salario}}</td>
			<td>
				<a href="{{ route('users.edit', $user) }}" class="btn btn-success"> <span class="glyphicon glyphicon-wrench
					" aria-hidden="true"></span></a> 


					{!! Form::open(['method' => 'DELETE','route' => ['users.destroy', $user],'style'=>'display:inline']) !!}
					{!! Form::submit('delete', ['class' => 'btn btn-danger']) !!}
					{!! Form::close() !!}


					<a href="{{ route('payment.pagoUsuario',$user) }}" class="btn btn-info"> <span class="glyphicon glyphicon-usd
						" aria-hidden="true"></span></a> 

						<a href="{{ route('payment.historial',$user) }}" class="btn btn-warning"> <span class="glyphicon glyphicon-eye-open
							" aria-hidden="true"></span></a> 

						</td>		
						@endforeach
					</tbody>

				</table>
				{!! $users->render() !!}
				@endsection()