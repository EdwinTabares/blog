
{{dd($user)}}
@extends('template.main')
@section('title', 'Crear Usuario')

@section('content')

{!! Form::open(['route' => 'users.store', 'method' => 'POST']) !!}

<div class="form-group">
	{!! Form::label('nombre', 'Nombre') !!}
	{!! Form::text('nombre', null, ['class' =>'form-control', 'placeholder' => 'Nombre Completo', 'required']) !!}
</div>

<div class="form-group">
	{!! Form::label('apellido', 'Apellido') !!}
	{!! Form::text('apellido', null, ['class' =>'form-control', 'placeholder' => 'Apellido', 'required']) !!}
</div>


<div class="form-group">
	{!! Form::label('tipo documento', 'Tipo Documento') !!}
	{!! Form::select('tipo_documento', [ 'cedulaC' => 'cedula ciudadania','cedulaE' => 'Cedula Extrangeria'], null, ['class' => 'form-control', 'placeholder' => 'Seleccione una opcion...', 'required']) !!}
</div>

<div class="form-group">
	{!! Form::label('numero documento', 'Numero Documento') !!}
	{!! Form::text('numero_documento', null, ['class' =>'form-control', 'placeholder' => 'numero de documento', 'required']) !!}
</div>

<div class="form-group">
	{!! Form::label('salario', 'salario') !!}
	{!! Form::text('salario', null, ['class' =>'form-control', 'placeholder' => 'salario', 'required']) !!}
</div>


<div class="form-group">
	{!! Form::submit('Registrar', ['class' => 'btn btn-primary']) !!}
</div>

{!! Form::close() !!}


@endsection