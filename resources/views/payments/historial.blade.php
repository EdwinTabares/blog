@extends('template.main')

@section('title', 'historial')

@section('content')

<table class="table table-striped">
	<thead>
		
		<th>pago</th>
		<th>fecha</th>
		
	</thead>
	<tbody>
		@foreach($pagosU as $pago)
		<tr>
			
			<td>{{ $pago->valor}}</td>
			<td>{{ $pago->fecha_pago}}</td>
			
		</tr>
		@endforeach
	</tbody>
	
</table>	
{!! $pagosU->render()!!}
@endsection()








